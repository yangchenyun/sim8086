package main

import (
    "bufio"
    "encoding/binary"
    "fmt"
    "os"
    "io"
	"errors"
)

// debug flags controled by build tag
var debug = false

// A table to look up op from byte code
var opCodeTable = map[uint8]string{
    0b00100010: "mov",
}

// define a enum for different mod mode
type modMode uint8

func (m modMode) String() string {
    switch m {
    case memoryModeNoDisp:
        return "memoryModeNoDisp"
    case memoryMode8BitDisp:
        return "memoryMode8BitDisp"
    case memoryMode16BitDisp:
        return "memoryMode16BitDisp"
    case registerModeNoDisp:
        return "registerModeNoDisp"
    }
    return "unknown"
}

const (
    memoryModeNoDisp modMode = iota
    memoryMode8BitDisp
    memoryMode16BitDisp
    registerModeNoDisp
)
    

// A table to look up mod from byte code
var modCodeTable = map[uint8]modMode {
    0b00: memoryModeNoDisp,
    0b01: memoryMode8BitDisp,
    0b10: memoryMode16BitDisp,
    0b11: registerModeNoDisp,
}

// regCodeTable maps a register code and word to a register name.
var regCodeTable = map[uint8][2]string{
    0b000: {"al", "ax"},
    0b001: {"cl", "cx"},
    0b010: {"dl", "dx"},
    0b011: {"bl", "bx"},
    0b100: {"ah", "sp"},
    0b101: {"ch", "bp"},
    0b110: {"dh", "si"},
    0b111: {"bh", "di"},
}
    

// MnemonicInstrut is a struct that represents an instruction in mnemonic form.
type MnemonicInstrut struct {
    op string
    operand1 string
    operand2 string
}

// String returns a string representation of the instruction.
func (inst MnemonicInstrut) String() string {
    str := inst.op
    if inst.operand1 != "" {
        str += " " + inst.operand1
    }
    if inst.operand2 != "" {
        str += "," + inst.operand2
    }
    return str
}

// FromByteInstruct builds a MneumonicInstrut from a ByteInstrut.
func FromByteInstruct(byteInst ByteInstrut) MnemonicInstrut {
    var mnemonic MnemonicInstrut
    mnemonic.op = opCodeTable[byteInst.opCode]
    reg := regCodeTable[byteInst.reg][byteInst.W]
    rm := regCodeTable[byteInst.rm][byteInst.W]

    // <op>, <dest>, <src>
    if byteInst.sourceInReg() {
        mnemonic.operand1 = rm
        mnemonic.operand2 = reg
    } else {
        mnemonic.operand1 = reg
        mnemonic.operand2 = rm
    }

    return mnemonic
}

type ByteInstrut struct {
    // 1st byte for opcode
    opCode uint8 // 6 bits
    D uint8  // 1bit
    W uint8  // 1bit

    // 2nd byte for params
    mod uint8 // 2 bits
    reg uint8 // 3 bits
    rm uint8 // 3 bits
}

func (inst ByteInstrut) String() string {
    str := fmt.Sprintf("opCode: %06b, ", inst.opCode)
    str += fmt.Sprintf("D: %01b, ", inst.D)
    str += fmt.Sprintf("W: %01b, ", inst.W)

    str += fmt.Sprintf("mod: %02b, ", inst.mod)
    str += fmt.Sprintf("reg: %03b, ", inst.reg)
    str += fmt.Sprintf("rm: %03b", inst.rm)
    return str
}

func (inst ByteInstrut) sourceInReg() bool {
    return inst.D == 0b00
}

func (inst ByteInstrut) destInReg() bool {
    return inst.D == 0b01
}

func boolToInt(b bool) int {
    if b {
        return 1
    }
    return 0
}

func parseByteInstrut(buf []byte) (ByteInstrut, error) {
    var instr ByteInstrut

    // Check if buffer has enough bytes
    if len(buf) < 2 {
        return instr, errors.New("buffer too short")
    }

    // Decode the instruction bytes
    // NOTE: OSX: sysctl -n hw.byteorder
    instrBytes := binary.BigEndian.Uint16(buf)

    // Big endian byte order
    first := uint8((instrBytes >> 8) & 0xff)
    second := uint8(instrBytes & 0xff)


    if debug {
        fmt.Printf("Parsing buf: %016b\n", instrBytes)
        fmt.Printf("first: %08b\n", first)
        fmt.Printf("second: %08b\n", second)
    }

    instr.opCode = uint8(first >> 2)
    instr.D = uint8((first >> 6) & 0x1)
    instr.W = uint8(first & 0x1)

    instr.mod = uint8(second >> 6)
    instr.reg = uint8((second >> 3) & 0x7)
    instr.rm = uint8(second & 0x7)

    return instr, nil
}

func main() {
    // Check if the file name is provided
    if len(os.Args) != 2 {
        fmt.Println("Usage: go run main.go <filename>")
        return
    }

    // Open the input file for reading
    file, err := os.Open(os.Args[1])
    if err != nil {
        fmt.Println(err)
        return
    }
    defer file.Close()

    // Create a reader to read in the bytes from the file
    reader := bufio.NewReader(file)

    // Read in the bytes from the file
    var buf []byte
    for {
        b, err := reader.ReadByte()
        if err != nil {
            if err == io.EOF {
                break
            }
            fmt.Println(err)
            return
        }
        buf = append(buf, b)
    }

    // Decode the bytes as 8086 instructions
    for i := 0; i < len(buf); i += 2 {
        if debug {
            fmt.Printf("Parsing buf: %08b\n", buf[i:i+2])
        }
        instr, _ := parseByteInstrut(buf[i:i+2])

        if debug {
            fmt.Println(instr)
        }
        fmt.Println(FromByteInstruct(instr))
    }
}
