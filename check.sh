#!/usr/bin/env sh

bin_file="$@"
f1=$(mktemp)
f2=$(mktemp)
go run main.go $bin_file  > "$f1"

# NOTE: 16 bit disassemble
ndisasm -b 16 $bin_file | awk '{print $3, $4}'  > "$f2"
diff -y "$f1" "$f2"
rm "$f1" "$f2"
